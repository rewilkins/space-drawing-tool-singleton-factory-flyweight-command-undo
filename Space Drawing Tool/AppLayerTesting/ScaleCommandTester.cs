﻿using System;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using AppLayer.Command;
using AppLayer.DrawingComponents;
using System.Collections.Generic;

namespace AppLayerTesting
{
    [TestClass]
    public class ScaleCommandTester
    {
        [TestMethod]
        public void ScaleCommand_ScaleLarger()
        {

            Drawing drawing = new Drawing();
            CommandFactory commandFactory = new CommandFactory() { TargetDrawing = drawing };
            drawing.objectFactory = new ObjectFactory() { ReferenceType = typeof(NewCommandTester) };
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Aliens.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Objects.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Moons.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Planets.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Ships.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Stars.{0}.png");

            // create the 4 objects
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Alien-01", Palette = 0, Location = new Point(10, 10), Size = new Size(80, 80) }, 0));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Moon-01", Palette = 1, Location = new Point(200, 310), Size = new Size(80, 80) }, 1));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Object-01", Palette = 2, Location = new Point(240, 150), Size = new Size(80, 80) }, 2));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Planets-01", Palette = 3, Location = new Point(350, 300), Size = new Size(80, 80) }, 3));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10, 10)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(200, 310)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(240, 150)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350, 300)));

            // this simulates selecting the 4 items
            List<_Object> list = new List<_Object>();
            list.Add(drawing.FindTreeAtPosition(new Point(10, 10)));
            list.Add(drawing.FindTreeAtPosition(new Point(200, 310)));
            list.Add(drawing.FindTreeAtPosition(new Point(240, 150)));
            list.Add(drawing.FindTreeAtPosition(new Point(350, 300)));

            // positive scale checking
            Point dragStart = new Point() { X = 0, Y = 0 };
            Point dragEnd = new Point() { X = 80, Y = 80 };

            // this moves all the selected items in "list"
            commandFactory.Create("scale", list, dragStart, dragEnd).Execute();
            Assert.AreEqual(new Size(160, 160), drawing.FindTreeAtPosition(new Point(10, 10)).Size);
            Assert.AreEqual(new Size(160, 160), drawing.FindTreeAtPosition(new Point(200, 310)).Size);
            Assert.AreEqual(new Size(160, 160), drawing.FindTreeAtPosition(new Point(240, 150)).Size);
            Assert.AreEqual(new Size(160, 160), drawing.FindTreeAtPosition(new Point(350, 300)).Size);
        }

        [TestMethod]
        public void ScaleCommand_ScaleSmaller()
        {

            Drawing drawing = new Drawing();
            CommandFactory commandFactory = new CommandFactory() { TargetDrawing = drawing };
            drawing.objectFactory = new ObjectFactory() { ReferenceType = typeof(NewCommandTester) };
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Aliens.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Objects.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Moons.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Planets.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Ships.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Stars.{0}.png");

            // create the 4 objects
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Alien-01", Palette = 0, Location = new Point(10, 10), Size = new Size(80, 80) }, 0));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Moon-01", Palette = 1, Location = new Point(200, 310), Size = new Size(80, 80) }, 1));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Object-01", Palette = 2, Location = new Point(240, 150), Size = new Size(80, 80) }, 2));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Planets-01", Palette = 3, Location = new Point(350, 300), Size = new Size(80, 80) }, 3));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10, 10)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(200, 310)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(240, 150)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350, 300)));

            // this simulates selecting the 4 items
            List<_Object> list = new List<_Object>();
            list.Add(drawing.FindTreeAtPosition(new Point(10, 10)));
            list.Add(drawing.FindTreeAtPosition(new Point(200, 310)));
            list.Add(drawing.FindTreeAtPosition(new Point(240, 150)));
            list.Add(drawing.FindTreeAtPosition(new Point(350, 300)));

            // negative scale checking
            Point dragStart = new Point() { X = 0, Y = 0 };
            Point dragEnd = new Point() { X = -40, Y = -40 };

            // this moves all the selected items in "list"
            commandFactory.Create("scale", list, dragStart, dragEnd).Execute();
            Assert.AreEqual(new Size(40, 40), drawing.FindTreeAtPosition(new Point(10, 10)).Size);
            Assert.AreEqual(new Size(40, 40), drawing.FindTreeAtPosition(new Point(200, 310)).Size);
            Assert.AreEqual(new Size(40, 40), drawing.FindTreeAtPosition(new Point(240, 150)).Size);
            Assert.AreEqual(new Size(40, 40), drawing.FindTreeAtPosition(new Point(350, 300)).Size);

        }

        [TestMethod]
        public void ScaleCommand_NoSelectedItems()
        {

            Drawing drawing = new Drawing();
            CommandFactory commandFactory = new CommandFactory() { TargetDrawing = drawing };
            drawing.objectFactory = new ObjectFactory() { ReferenceType = typeof(NewCommandTester) };
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Aliens.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Objects.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Moons.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Planets.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Ships.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Stars.{0}.png");

            // create the 4 objects
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Alien-01", Palette = 0, Location = new Point(10, 10), Size = new Size(80, 80) }, 0));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Moon-01", Palette = 1, Location = new Point(200, 310), Size = new Size(80, 80) }, 1));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Object-01", Palette = 2, Location = new Point(240, 150), Size = new Size(80, 80) }, 2));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Planets-01", Palette = 3, Location = new Point(350, 300), Size = new Size(80, 80) }, 3));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10, 10)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(200, 310)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(240, 150)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350, 300)));

            // this simulates selecting the 4 items
            List<_Object> list = new List<_Object>();

            // positive scale checking
            Point dragStart = new Point() { X = 0, Y = 0 };
            Point dragEnd = new Point() { X = 80, Y = 80 };

            // this moves all the selected items in "list"
            commandFactory.Create("scale", list, dragStart, dragEnd).Execute();
            Assert.AreEqual(new Size(80, 80), drawing.FindTreeAtPosition(new Point(10, 10)).Size);
            Assert.AreEqual(new Size(80, 80), drawing.FindTreeAtPosition(new Point(200, 310)).Size);
            Assert.AreEqual(new Size(80, 80), drawing.FindTreeAtPosition(new Point(240, 150)).Size);
            Assert.AreEqual(new Size(80, 80), drawing.FindTreeAtPosition(new Point(350, 300)).Size);

        }

        [TestMethod]
        public void ScaleCommand_undo()
        {

            Drawing drawing = new Drawing();
            CommandFactory commandFactory = new CommandFactory() { TargetDrawing = drawing };
            drawing.objectFactory = new ObjectFactory() { ReferenceType = typeof(NewCommandTester) };
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Aliens.{0}.png");

            // create the 1 objects
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Alien-01", Palette = 0, Location = new Point(10, 10), Size = new Size(80, 80) }, 0));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10, 10)));

            // this simulates selecting the 4 items
            List<_Object> list = new List<_Object>();
            list.Add(drawing.FindTreeAtPosition(new Point(10, 10)));

            // positive scale checking
            Point dragStart = new Point() { X = 0, Y = 0 };
            Point dragEnd = new Point() { X = 80, Y = 80 };

            // this moves all the selected items in "list"
            Command command = commandFactory.Create("scale", list, dragStart, dragEnd);
            Assert.IsNotNull(command);
            command.Execute();
            Assert.AreEqual(new Size(160, 160), drawing.FindTreeAtPosition(new Point(10, 10)).Size);

            // testing undo
            command.undo();
            Assert.AreEqual(new Size(80, 80), drawing.FindTreeAtPosition(new Point(10, 10)).Size);

        }
    }
}
