﻿using System;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using AppLayer.Command;
using AppLayer.DrawingComponents;

namespace AppLayerTesting
{
    [TestClass]
    public class NewCommandTester
    {
        [TestMethod]
        public void NewCommand_NonEmptyDrawing()
        {
            // Setup a drawing
            ObjectFactory objectFactory = new ObjectFactory() {ReferenceType = typeof(NewCommandTester)};
            objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Aliens.{0}.png");
            objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Objects.{0}.png");
            objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Moons.{0}.png");
            objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Planets.{0}.png");

            Drawing drawing = new Drawing();
            CommandFactory commandFactory = new CommandFactory() {TargetDrawing = drawing};
            drawing.Add(objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Alien-01", Palette = 0, Location = new Point(10,10), Size  = new Size(80, 80)}, 0));
            drawing.Add(objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Moon-01", Palette = 1, Location = new Point(200,310), Size = new Size(80, 80)}, 1));
            drawing.Add(objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Object-01", Palette = 2, Location = new Point(240, 150), Size = new Size(80, 80)}, 2));
            drawing.Add(objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Planets-01", Palette = 3, Location = new Point(350, 300), Size = new Size(80, 80)}, 3));
            Assert.AreEqual(4, drawing.TreeCount);

            // setup a New command
            Command newCmd = commandFactory.Create("new");
            Assert.IsNotNull(newCmd);

            // Stimulate (Execute newCmd.Execute)
            newCmd.Execute();

            // Assert the predicated results
            Assert.AreEqual(0, drawing.TreeCount);            
        }

        [TestMethod]
        public void NewCommand_EmptyDrawing()
        {
            Drawing drawing = new Drawing();
            CommandFactory commandFactory = new CommandFactory() { TargetDrawing = drawing };

            Assert.AreEqual(0, drawing.TreeCount);

            Command newCmd = commandFactory.Create("new");
            Assert.IsNotNull(newCmd);
            newCmd.Execute();
            Assert.AreEqual(0, drawing.TreeCount);

        }

        [TestMethod]
        public void NewCommand_NoDrawing()
        {
            CommandFactory commandFactory = new CommandFactory() { TargetDrawing = null };

            Command newCmd = commandFactory.Create("new");
            Assert.IsNotNull(newCmd);
            newCmd.Execute();
            // This didn't throw an exception, then it worked as expected
        }

    }
}
