﻿using System;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using AppLayer.Command;
using AppLayer.DrawingComponents;
using System.Collections.Generic;

namespace AppLayerTesting
{
    [TestClass]
    public class CommandProcessorTester
    {
        [TestMethod]
        public void CommandProcessor_SingletonSingleness()
        {
            Assert.AreEqual(CommandProcessor_Singleton.getInstance(), CommandProcessor_Singleton.getInstance());
            Assert.AreEqual(CommandProcessor_Singleton.getInstance(), CommandProcessor_Singleton.getInstance());
            Assert.AreEqual(CommandProcessor_Singleton.getInstance(), CommandProcessor_Singleton.getInstance());
            Assert.AreEqual(CommandProcessor_Singleton.getInstance(), CommandProcessor_Singleton.getInstance());
            Assert.AreEqual(CommandProcessor_Singleton.getInstance(), CommandProcessor_Singleton.getInstance());
        }

        [TestMethod]
        public void CommandProcessor_processCommand()
        {
            Drawing drawing = new Drawing();
            CommandFactory commandFactory = new CommandFactory() { TargetDrawing = drawing };
            drawing.objectFactory = new ObjectFactory() { ReferenceType = typeof(NewCommandTester) };
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Aliens.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Objects.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Moons.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Planets.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Ships.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Stars.{0}.png");

            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Alien-01", Palette = 0, Location = new Point(10, 10), Size = new Size(80, 80) }, 0));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Moon-01", Palette = 1, Location = new Point(200, 310), Size = new Size(80, 80) }, 1));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Object-01", Palette = 2, Location = new Point(240, 150), Size = new Size(80, 80) }, 2));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Planets-01", Palette = 3, Location = new Point(350, 300), Size = new Size(80, 80) }, 3));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10, 10)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(200, 310)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(240, 150)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350, 300)));

            // this simulates selecting the 4 items
            List<_Object> list = new List<_Object>();

            // positive translation checking
            int mod1 = 100;
            int mod2 = 100;
            Point locVec = new Point() { X = mod1, Y = mod2 };

            // this moves all the selected items in "list"
            list.Clear();
            list.Add(drawing.FindTreeAtPosition(new Point(10, 10)));
            CommandProcessor_Singleton.getInstance().processCommand(commandFactory.Create("move", list, locVec));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10 + mod1, 10 + mod2)));

            list.Clear();
            list.Add(drawing.FindTreeAtPosition(new Point(200, 310)));
            CommandProcessor_Singleton.getInstance().processCommand(commandFactory.Create("move", list, locVec));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(200 + mod1, 310 + mod2)));

            list.Clear();
            list.Add(drawing.FindTreeAtPosition(new Point(240, 150)));
            CommandProcessor_Singleton.getInstance().processCommand(commandFactory.Create("move", list, locVec));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(240 + mod1, 150 + mod2)));

            list.Clear();
            list.Add(drawing.FindTreeAtPosition(new Point(350, 300)));
            CommandProcessor_Singleton.getInstance().processCommand(commandFactory.Create("move", list, locVec));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350 + mod1, 300 + mod2)));
        }

        [TestMethod]
        public void CommandProcessor_processCommand_Undo()
        {
            Drawing drawing = new Drawing();
            CommandFactory commandFactory = new CommandFactory() { TargetDrawing = drawing };
            drawing.objectFactory = new ObjectFactory() { ReferenceType = typeof(NewCommandTester) };
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Aliens.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Objects.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Moons.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Planets.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Ships.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Stars.{0}.png");

            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Alien-01", Palette = 0, Location = new Point(10, 10), Size = new Size(80, 80) }, 0));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Moon-01", Palette = 1, Location = new Point(200, 310), Size = new Size(80, 80) }, 1));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Object-01", Palette = 2, Location = new Point(240, 150), Size = new Size(80, 80) }, 2));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Planets-01", Palette = 3, Location = new Point(350, 300), Size = new Size(80, 80) }, 3));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10, 10)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(200, 310)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(240, 150)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350, 300)));

            // this simulates selecting the 4 items
            List<_Object> list = new List<_Object>();

            // positive translation checking
            int mod1 = 100;
            int mod2 = 100;
            Point locVec = new Point() { X = mod1, Y = mod2 };

            // this moves all the selected items in "list"
            list.Clear();
            list.Add(drawing.FindTreeAtPosition(new Point(10, 10)));
            CommandProcessor_Singleton.getInstance().processCommand(commandFactory.Create("move", list, locVec));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10 + mod1, 10 + mod2)));

            list.Clear();
            list.Add(drawing.FindTreeAtPosition(new Point(200, 310)));
            CommandProcessor_Singleton.getInstance().processCommand(commandFactory.Create("move", list, locVec));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(200 + mod1, 310 + mod2)));

            list.Clear();
            list.Add(drawing.FindTreeAtPosition(new Point(240, 150)));
            CommandProcessor_Singleton.getInstance().processCommand(commandFactory.Create("move", list, locVec));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(240 + mod1, 150 + mod2)));

            list.Clear();
            list.Add(drawing.FindTreeAtPosition(new Point(350, 300)));
            CommandProcessor_Singleton.getInstance().processCommand(commandFactory.Create("move", list, locVec));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350 + mod1, 300 + mod2)));

            // test undo stack
            CommandProcessor_Singleton.getInstance().undoCommand();
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10 + mod1, 10 + mod2)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(200 + mod1, 310 + mod2)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(240 + mod1, 150 + mod2)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350, 300)));

            CommandProcessor_Singleton.getInstance().undoCommand();
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10 + mod1, 10 + mod2)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(200 + mod1, 310 + mod2)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(240, 150)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350, 300)));

            CommandProcessor_Singleton.getInstance().undoCommand();
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10 + mod1, 10 + mod2)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(200, 310)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(240, 150)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350, 300)));

            CommandProcessor_Singleton.getInstance().undoCommand();
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10, 10)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(200, 310)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(240, 150)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350, 300)));

        }

        [TestMethod]
        public void ScaleCommand_redo()
        {
            Drawing drawing = new Drawing();
            CommandFactory commandFactory = new CommandFactory() { TargetDrawing = drawing };
            drawing.objectFactory = new ObjectFactory() { ReferenceType = typeof(NewCommandTester) };
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Aliens.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Objects.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Moons.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Planets.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Ships.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Stars.{0}.png");

            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Alien-01", Palette = 0, Location = new Point(10, 10), Size = new Size(80, 80) }, 0));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Moon-01", Palette = 1, Location = new Point(200, 310), Size = new Size(80, 80) }, 1));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Object-01", Palette = 2, Location = new Point(240, 150), Size = new Size(80, 80) }, 2));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Planets-01", Palette = 3, Location = new Point(350, 300), Size = new Size(80, 80) }, 3));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10, 10)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(200, 310)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(240, 150)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350, 300)));

            // this simulates selecting the 4 items
            List<_Object> list = new List<_Object>();

            // positive translation checking
            int mod1 = 100;
            int mod2 = 100;
            Point locVec = new Point() { X = mod1, Y = mod2 };

            // this moves all the selected items in "list"
            list.Clear();
            list.Add(drawing.FindTreeAtPosition(new Point(10, 10)));
            CommandProcessor_Singleton.getInstance().processCommand(commandFactory.Create("move", list, locVec));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10 + mod1, 10 + mod2)));

            list.Clear();
            list.Add(drawing.FindTreeAtPosition(new Point(200, 310)));
            CommandProcessor_Singleton.getInstance().processCommand(commandFactory.Create("move", list, locVec));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(200 + mod1, 310 + mod2)));

            list.Clear();
            list.Add(drawing.FindTreeAtPosition(new Point(240, 150)));
            CommandProcessor_Singleton.getInstance().processCommand(commandFactory.Create("move", list, locVec));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(240 + mod1, 150 + mod2)));

            list.Clear();
            list.Add(drawing.FindTreeAtPosition(new Point(350, 300)));
            CommandProcessor_Singleton.getInstance().processCommand(commandFactory.Create("move", list, locVec));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350 + mod1, 300 + mod2)));

            // test undo stack
            CommandProcessor_Singleton.getInstance().undoCommand();
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10 + mod1, 10 + mod2)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(200 + mod1, 310 + mod2)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(240 + mod1, 150 + mod2)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350, 300)));

            CommandProcessor_Singleton.getInstance().undoCommand();
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10 + mod1, 10 + mod2)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(200 + mod1, 310 + mod2)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(240, 150)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350, 300)));

            CommandProcessor_Singleton.getInstance().undoCommand();
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10 + mod1, 10 + mod2)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(200, 310)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(240, 150)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350, 300)));

            CommandProcessor_Singleton.getInstance().undoCommand();
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10, 10)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(200, 310)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(240, 150)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350, 300)));


            // test redo stack
            CommandProcessor_Singleton.getInstance().redoCommand();
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10 + mod1, 10 + mod2)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(200, 310)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(240, 150)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350, 300)));

            CommandProcessor_Singleton.getInstance().redoCommand();
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10 + mod1, 10 + mod2)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(200 + mod1, 310 + mod2)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(240, 150)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350, 300)));

            CommandProcessor_Singleton.getInstance().redoCommand();
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10 + mod1, 10 + mod2)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(200 + mod1, 310 + mod2)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(240 + mod1, 150 + mod2)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350, 300)));

            CommandProcessor_Singleton.getInstance().redoCommand();
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10 + mod1, 10 + mod2)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(200 + mod1, 310 + mod2)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(240 + mod1, 150 + mod2)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350 + mod1, 300 + mod2)));

        }

        [TestMethod]
        public void ScaleCommand_clearRedoStack()
        {
            Drawing drawing = new Drawing();
            CommandFactory commandFactory = new CommandFactory() { TargetDrawing = drawing };
            drawing.objectFactory = new ObjectFactory() { ReferenceType = typeof(NewCommandTester) };
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Aliens.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Objects.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Moons.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Planets.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Ships.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Stars.{0}.png");

            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Alien-01", Palette = 0, Location = new Point(10, 10), Size = new Size(80, 80) }, 0));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Moon-01", Palette = 1, Location = new Point(200, 310), Size = new Size(80, 80) }, 1));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Object-01", Palette = 2, Location = new Point(240, 150), Size = new Size(80, 80) }, 2));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Planets-01", Palette = 3, Location = new Point(350, 300), Size = new Size(80, 80) }, 3));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10, 10)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(200, 310)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(240, 150)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350, 300)));

            // this simulates selecting the 4 items
            List<_Object> list = new List<_Object>();

            // positive translation checking
            int mod1 = 100;
            int mod2 = 100;
            Point locVec = new Point() { X = mod1, Y = mod2 };

            // this moves all the selected items in "list"
            list.Clear();
            list.Add(drawing.FindTreeAtPosition(new Point(10, 10)));
            CommandProcessor_Singleton.getInstance().processCommand(commandFactory.Create("move", list, locVec));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10 + mod1, 10 + mod2)));

            list.Clear();
            list.Add(drawing.FindTreeAtPosition(new Point(200, 310)));
            CommandProcessor_Singleton.getInstance().processCommand(commandFactory.Create("move", list, locVec));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(200 + mod1, 310 + mod2)));

            list.Clear();
            list.Add(drawing.FindTreeAtPosition(new Point(240, 150)));
            CommandProcessor_Singleton.getInstance().processCommand(commandFactory.Create("move", list, locVec));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(240 + mod1, 150 + mod2)));

            list.Clear();
            list.Add(drawing.FindTreeAtPosition(new Point(350, 300)));
            CommandProcessor_Singleton.getInstance().processCommand(commandFactory.Create("move", list, locVec));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350 + mod1, 300 + mod2)));

            // test undo stack
            CommandProcessor_Singleton.getInstance().undoCommand();
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10 + mod1, 10 + mod2)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(200 + mod1, 310 + mod2)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(240 + mod1, 150 + mod2)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350, 300)));

            CommandProcessor_Singleton.getInstance().undoCommand();
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10 + mod1, 10 + mod2)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(200 + mod1, 310 + mod2)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(240, 150)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350, 300)));

            CommandProcessor_Singleton.getInstance().undoCommand();
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10 + mod1, 10 + mod2)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(200, 310)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(240, 150)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350, 300)));

            CommandProcessor_Singleton.getInstance().undoCommand();
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10, 10)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(200, 310)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(240, 150)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350, 300)));

            // performing another command after undos will clear the redo stack
            mod1 = 200;
            mod2 = 200;
            locVec = new Point() { X = mod1, Y = mod2 };
            list.Clear();
            list.Add(drawing.FindTreeAtPosition(new Point(350, 300)));
            CommandProcessor_Singleton.getInstance().processCommand(commandFactory.Create("move", list, locVec));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350 + mod1, 300 + mod2)));
            mod1 = 100;
            mod2 = 100;
            locVec = new Point() { X = mod1, Y = mod2 };

            // test redo stack and make sure it is cleared
            CommandProcessor_Singleton.getInstance().redoCommand();
            Assert.IsNull(drawing.FindTreeAtPosition(new Point(10 + mod1, 10 + mod2)));

            CommandProcessor_Singleton.getInstance().redoCommand();
            Assert.IsNull(drawing.FindTreeAtPosition(new Point(200 + mod1, 310 + mod2)));

            CommandProcessor_Singleton.getInstance().redoCommand();
            Assert.IsNull(drawing.FindTreeAtPosition(new Point(240 + mod1, 150 + mod2)));

            CommandProcessor_Singleton.getInstance().redoCommand();
            Assert.IsNull(drawing.FindTreeAtPosition(new Point(350 + mod1, 300 + mod2)));

        }
    }
}
