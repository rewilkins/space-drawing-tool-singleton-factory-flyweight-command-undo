﻿using System;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using AppLayer.Command;
using AppLayer.DrawingComponents;
using System.Collections.Generic;

namespace AppLayerTesting
{
    [TestClass]
    public class MoveCommandTester
    {
        [TestMethod]
        public void MoveCommand_MovePositive()
        {

            Drawing drawing = new Drawing();
            CommandFactory commandFactory = new CommandFactory() { TargetDrawing = drawing };
            drawing.objectFactory = new ObjectFactory() { ReferenceType = typeof(NewCommandTester) };
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Aliens.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Objects.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Moons.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Planets.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Ships.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Stars.{0}.png");

            // create the 4 objects
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Alien-01", Palette = 0, Location = new Point(10, 10), Size = new Size(80, 80) }, 0));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Moon-01", Palette = 1, Location = new Point(200, 310), Size = new Size(80, 80) }, 1));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Object-01", Palette = 2, Location = new Point(240, 150), Size = new Size(80, 80) }, 2));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Planets-01", Palette = 3, Location = new Point(350, 300), Size = new Size(80, 80) }, 3));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10, 10)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(200, 310)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(240, 150)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350, 300)));

            // this simulates selecting the 4 items
            List<_Object> list = new List<_Object>();
            list.Add(drawing.FindTreeAtPosition(new Point(10, 10)));
            list.Add(drawing.FindTreeAtPosition(new Point(200, 310)));
            list.Add(drawing.FindTreeAtPosition(new Point(240, 150)));
            list.Add(drawing.FindTreeAtPosition(new Point(350, 300)));

            // positive translation checking
            int mod1 = 25;
            int mod2 = 25;
            Point locVec = new Point() { X = mod1, Y = mod2 };

            // this moves all the selected items in "list"
            commandFactory.Create("move", list, locVec).Execute();
            Assert.IsNull(drawing.FindTreeAtPosition(new Point(10, 10)));
            Assert.IsNull(drawing.FindTreeAtPosition(new Point(200, 310)));
            Assert.IsNull(drawing.FindTreeAtPosition(new Point(240, 150)));
            Assert.IsNull(drawing.FindTreeAtPosition(new Point(350, 300)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10 + mod1, 10 + mod2)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(200 + mod1, 310 + mod2)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(240 + mod1, 150 + mod2)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350 + mod1, 300 + mod2)));

        }

        [TestMethod]
        public void MoveCommand_MoveNegative()
        {

            Drawing drawing = new Drawing();
            CommandFactory commandFactory = new CommandFactory() { TargetDrawing = drawing };
            drawing.objectFactory = new ObjectFactory() { ReferenceType = typeof(NewCommandTester) };
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Aliens.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Objects.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Moons.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Planets.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Ships.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Stars.{0}.png");

            // create the 4 objects
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Alien-01", Palette = 0, Location = new Point(10, 10), Size = new Size(80, 80) }, 0));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Moon-01", Palette = 1, Location = new Point(200, 310), Size = new Size(80, 80) }, 1));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Object-01", Palette = 2, Location = new Point(240, 150), Size = new Size(80, 80) }, 2));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Planets-01", Palette = 3, Location = new Point(350, 300), Size = new Size(80, 80) }, 3));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10, 10)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(200, 310)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(240, 150)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350, 300)));

            // this simulates selecting the 4 items
            List<_Object> list = new List<_Object>();
            list.Add(drawing.FindTreeAtPosition(new Point(10, 10)));
            list.Add(drawing.FindTreeAtPosition(new Point(200, 310)));
            list.Add(drawing.FindTreeAtPosition(new Point(240, 150)));
            list.Add(drawing.FindTreeAtPosition(new Point(350, 300)));

            // negative translation checking
            int mod1 = -25;
            int mod2 = -25;
            Point locVec = new Point() { X = mod1, Y = mod2 };

            // this moves all the selected items in "list"
            commandFactory.Create("move", list, locVec).Execute();
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10 - mod1, 10 - mod2)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(200 - mod1, 310 - mod2)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(240 - mod1, 150 - mod2)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350 - mod1, 300 - mod2)));

        }

        [TestMethod]
        public void MoveCommand_NoSelectedItems()
        {

            Drawing drawing = new Drawing();
            CommandFactory commandFactory = new CommandFactory() { TargetDrawing = drawing };
            drawing.objectFactory = new ObjectFactory() { ReferenceType = typeof(NewCommandTester) };
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Aliens.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Objects.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Moons.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Planets.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Ships.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Stars.{0}.png");

            // create the 4 objects
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Alien-01", Palette = 0, Location = new Point(10, 10), Size = new Size(80, 80) }, 0));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Moon-01", Palette = 1, Location = new Point(200, 310), Size = new Size(80, 80) }, 1));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Object-01", Palette = 2, Location = new Point(240, 150), Size = new Size(80, 80) }, 2));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Planets-01", Palette = 3, Location = new Point(350, 300), Size = new Size(80, 80) }, 3));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10, 10)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(200, 310)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(240, 150)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350, 300)));

            List<_Object> list = new List<_Object>();

            // positive translation checking
            int mod1 = 25;
            int mod2 = 25;
            Point locVec = new Point() { X = mod1, Y = mod2 };

            // this moves all the selected items in "list"
            commandFactory.Create("move", list, locVec).Execute();
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10, 10)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(200, 310)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(240, 150)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350, 300)));

        }

        [TestMethod]
        public void MoveCommand_undo()
        {

            Drawing drawing = new Drawing();
            CommandFactory commandFactory = new CommandFactory() { TargetDrawing = drawing };
            drawing.objectFactory = new ObjectFactory() { ReferenceType = typeof(NewCommandTester) };
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Aliens.{0}.png");

            // create the 1 objects
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Alien-01", Palette = 0, Location = new Point(10, 10), Size = new Size(80, 80) }, 0));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10, 10)));

            // select 1 trees
            drawing.FindTreeAtPosition(new Point(10, 10)).IsSelected = true;

            // this simulates selecting the 4 items
            List<_Object> list = new List<_Object>();
            list.Add(drawing.FindTreeAtPosition(new Point(10, 10)));

            // aditional setup...
            drawing.FindTreeAtPosition(new Point(10, 10))._ObjectType = "Alien-01";
            drawing.FindTreeAtPosition(new Point(10, 10)).TargetDrawing = drawing;

            // positive translation checking
            int mod1 = 25;
            int mod2 = 25;
            Point locVec = new Point() { X = mod1, Y = mod2 };

            // this moves all the selected items in "list"
            Command command = commandFactory.Create("move", list, locVec);
            Assert.IsNotNull(command);
            command.Execute();
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10 + mod1, 10 + mod2)));

            // testing undo
            command.undo();
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10, 10)));

        }

    }
}
