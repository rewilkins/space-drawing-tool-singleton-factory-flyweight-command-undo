﻿using System;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using AppLayer.Command;
using AppLayer.DrawingComponents;

namespace AppLayerTesting
{
    [TestClass]
    public class AddCommandTester
    {
        [TestMethod]
        public void AddCommand_NonEmptyDrawing()
        {

            Drawing drawing = new Drawing();
            CommandFactory commandFactory = new CommandFactory() { TargetDrawing = drawing };
            drawing.objectFactory = new ObjectFactory() { ReferenceType = typeof(NewCommandTester) };
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Aliens.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Objects.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Moons.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Planets.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Ships.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Stars.{0}.png");

            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Alien-01", Palette = 0, Location = new Point(10, 10), Size = new Size(80, 80) }, 0));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Moon-01", Palette = 1, Location = new Point(200, 310), Size = new Size(80, 80) }, 1));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Object-01", Palette = 2, Location = new Point(240, 150), Size = new Size(80, 80) }, 2));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Planets-01", Palette = 3, Location = new Point(350, 300), Size = new Size(80, 80) }, 3));
            Assert.AreEqual(4, drawing.TreeCount);

            commandFactory.Create("add", "Alien-02", 0, new Point(100, 100), 1).Execute();
            commandFactory.Create("add", "Moon-02", 1, new Point(250, 350), 1).Execute();
            commandFactory.Create("add", "Object-02", 2, new Point(340, 250), 1).Execute();
            commandFactory.Create("add", "Planets-02", 3, new Point(450, 400), 1).Execute();
            Assert.AreEqual(8, drawing.TreeCount);

            commandFactory.Create("add", "Alien-03", 0, new Point(200, 200), 1).Execute();
            commandFactory.Create("add", "Moon-03", 1, new Point(350, 450), 1).Execute();
            commandFactory.Create("add", "Object-03", 2, new Point(440, 350), 1).Execute();
            commandFactory.Create("add", "Planets-03", 3, new Point(550, 500), 1).Execute();
            Assert.AreEqual(12, drawing.TreeCount);

        }

        [TestMethod]
        public void AddCommand_EmptyDrawing()
        {

            Drawing drawing = new Drawing();
            CommandFactory commandFactory = new CommandFactory() { TargetDrawing = drawing };
            drawing.objectFactory = new ObjectFactory() { ReferenceType = typeof(NewCommandTester) };
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Aliens.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Objects.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Moons.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Planets.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Ships.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Stars.{0}.png");

            commandFactory.Create("add", "Alien-01", 0, new Point(10, 10), 1).Execute();
            commandFactory.Create("add", "Moon-01", 1, new Point(200, 310), 1).Execute();
            commandFactory.Create("add", "Object-01", 2, new Point(240, 150), 1).Execute();
            commandFactory.Create("add", "Planets-01", 3, new Point(350, 300), 1).Execute();
            Assert.AreEqual(4, drawing.TreeCount);

            commandFactory.Create("add", "Alien-02", 0, new Point(100, 100), 1).Execute();
            commandFactory.Create("add", "Moon-02", 1, new Point(250, 350), 1).Execute();
            commandFactory.Create("add", "Object-02", 2, new Point(340, 250), 1).Execute();
            commandFactory.Create("add", "Planets-02", 3, new Point(450, 400), 1).Execute();
            Assert.AreEqual(8, drawing.TreeCount);

            commandFactory.Create("add", "Alien-03", 0, new Point(200, 200), 1).Execute();
            commandFactory.Create("add", "Moon-03", 1, new Point(350, 450), 1).Execute();
            commandFactory.Create("add", "Object-03", 2, new Point(440, 350), 1).Execute();
            commandFactory.Create("add", "Planets-03", 3, new Point(550, 500), 1).Execute();
            Assert.AreEqual(12, drawing.TreeCount);

        }

        [TestMethod]
        public void AddCommand_NoDrawing()
        {
            CommandFactory commandFactory = new CommandFactory() { TargetDrawing = null };

            Command Cmd1 = commandFactory.Create("add", "Alien-01", 0, new Point(10, 10), 1);
            Command Cmd2 = commandFactory.Create("add", "Moon-01", 1, new Point(200, 310), 1);
            Command Cmd3 = commandFactory.Create("add", "Object-01", 2, new Point(240, 150), 1);
            Command Cmd4 = commandFactory.Create("add", "Planets-01", 3, new Point(350, 300), 1);

            Assert.IsNotNull(Cmd1);
            Assert.IsNotNull(Cmd2);
            Assert.IsNotNull(Cmd3);
            Assert.IsNotNull(Cmd4);

            Cmd1.Execute();
            Cmd2.Execute();
            Cmd3.Execute();
            Cmd4.Execute();
            // This didn't throw an exception, then it worked as expected
        }

        [TestMethod]
        public void DuplicateCommand_undo()
        {

            Drawing drawing = new Drawing();
            CommandFactory commandFactory = new CommandFactory() { TargetDrawing = drawing };
            drawing.objectFactory = new ObjectFactory() { ReferenceType = typeof(NewCommandTester) };
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Aliens.{0}.png");

            // Add 1 item
            Command command = commandFactory.Create("add", "Alien-01", 0, new Point(10, 10), 1);
            Assert.IsNotNull(command);
            command.Execute();
            Assert.AreEqual(1, drawing.TreeCount);

            // testing undo
            command.undo();
            Assert.IsNull(drawing.FindTreeAtPosition(new Point(10, 10)));
            Assert.AreEqual(0, drawing.TreeCount);

        }

    }
}
