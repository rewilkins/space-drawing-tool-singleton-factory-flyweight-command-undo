﻿using System;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using AppLayer.Command;
using AppLayer.DrawingComponents;
using System.Collections.Generic;

namespace AppLayerTesting
{
    [TestClass]
    public class RemoveSelectedCommandTester
    {
        [TestMethod]
        public void RemoveSelectedCommand_SelectedItems()
        {

            Drawing drawing = new Drawing();
            CommandFactory commandFactory = new CommandFactory() { TargetDrawing = drawing };
            drawing.objectFactory = new ObjectFactory() { ReferenceType = typeof(NewCommandTester) };
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Aliens.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Objects.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Moons.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Planets.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Ships.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Stars.{0}.png");

            // create the 4 objects
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Alien-01", Palette = 0, Location = new Point(10, 10), Size = new Size(80, 80) }, 0));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Moon-01", Palette = 1, Location = new Point(200, 310), Size = new Size(80, 80) }, 1));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Object-01", Palette = 2, Location = new Point(240, 150), Size = new Size(80, 80) }, 2));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Planets-01", Palette = 3, Location = new Point(350, 300), Size = new Size(80, 80) }, 3));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10, 10)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(200, 310)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(240, 150)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350, 300)));

            Assert.AreEqual(4, drawing.TreeCount);

            // select 2 trees
            drawing.FindTreeAtPosition(new Point(10, 10)).IsSelected = true;
            drawing.FindTreeAtPosition(new Point(200, 310)).IsSelected = true;

            // this removes all the selected items
            commandFactory.Create("remove").Execute();
            Assert.IsNull(drawing.FindTreeAtPosition(new Point(10, 10)));
            Assert.IsNull(drawing.FindTreeAtPosition(new Point(200, 310)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(240, 150)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350, 300)));

            Assert.AreEqual(2, drawing.TreeCount);


        }

        [TestMethod]
        public void RemoveSelectedCommand_NoSelectedItems()
        {

            Drawing drawing = new Drawing();
            CommandFactory commandFactory = new CommandFactory() { TargetDrawing = drawing };
            drawing.objectFactory = new ObjectFactory() { ReferenceType = typeof(NewCommandTester) };
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Aliens.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Objects.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Moons.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Planets.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Ships.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Stars.{0}.png");

            // create the 4 objects
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Alien-01", Palette = 0, Location = new Point(10, 10), Size = new Size(80, 80) }, 0));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Moon-01", Palette = 1, Location = new Point(200, 310), Size = new Size(80, 80) }, 1));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Object-01", Palette = 2, Location = new Point(240, 150), Size = new Size(80, 80) }, 2));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Planets-01", Palette = 3, Location = new Point(350, 300), Size = new Size(80, 80) }, 3));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10, 10)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(200, 310)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(240, 150)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350, 300)));

            Assert.AreEqual(4, drawing.TreeCount);

            // this removes all the selected items
            commandFactory.Create("remove").Execute();
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10, 10)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(200, 310)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(240, 150)));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(350, 300)));

            Assert.AreEqual(4, drawing.TreeCount);

        }

        [TestMethod]
        public void RemoveSelectedCommand_undo()
        {

            Drawing drawing = new Drawing();
            CommandFactory commandFactory = new CommandFactory() { TargetDrawing = drawing };
            drawing.objectFactory = new ObjectFactory() { ReferenceType = typeof(NewCommandTester) };
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Aliens.{0}.png");

            // create the 1 objects
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Alien-01", Palette = 0, Location = new Point(10, 10), Size = new Size(80, 80) }, 0));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10, 10)));
            Assert.AreEqual(1, drawing.TreeCount);

            // select 1 trees
            drawing.FindTreeAtPosition(new Point(10, 10)).IsSelected = true;

            // aditional setup...
            drawing.FindTreeAtPosition(new Point(10, 10))._ObjectType = "Alien-01";
            drawing.FindTreeAtPosition(new Point(10, 10)).TargetDrawing = drawing;

            // this removes all the selected items
            Command command = commandFactory.Create("remove");
            Assert.IsNotNull(command);
            command.Execute();
            Assert.IsNull(drawing.FindTreeAtPosition(new Point(10, 10)));
            Assert.AreEqual(0, drawing.TreeCount);

            // testing undo
            command.undo();
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10, 10)));
            Assert.AreEqual(1, drawing.TreeCount);

        }
    }
}
