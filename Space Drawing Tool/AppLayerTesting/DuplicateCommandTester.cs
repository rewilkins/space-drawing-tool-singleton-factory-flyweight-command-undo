﻿using System;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using AppLayer.Command;
using AppLayer.DrawingComponents;
using System.Collections.Generic;

namespace AppLayerTesting
{
    [TestClass]
    public class DuplicateCommandTester
    {
        [TestMethod]
        public void DuplicateCommand_SelectedItems()
        {

            Drawing drawing = new Drawing();
            CommandFactory commandFactory = new CommandFactory() { TargetDrawing = drawing };
            drawing.objectFactory = new ObjectFactory() { ReferenceType = typeof(NewCommandTester) };
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Aliens.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Objects.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Moons.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Planets.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Ships.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Stars.{0}.png");

            // create the 4 objects
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Alien-01", Palette = 0, Location = new Point(10, 10), Size = new Size(80, 80) }, 0));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Moon-01", Palette = 1, Location = new Point(200, 310), Size = new Size(80, 80) }, 1));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Object-01", Palette = 2, Location = new Point(240, 150), Size = new Size(80, 80) }, 2));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Planets-01", Palette = 3, Location = new Point(350, 300), Size = new Size(80, 80) }, 3));

            // this simulates selecting the 4 items
            List<_Object> list = new List<_Object>();
            list.Add(drawing.FindTreeAtPosition(new Point(10, 10)));
            list.Add(drawing.FindTreeAtPosition(new Point(200, 310)));
            list.Add(drawing.FindTreeAtPosition(new Point(240, 150)));
            list.Add(drawing.FindTreeAtPosition(new Point(350, 300)));

            // duplication command actually needs the items to be selected
            list[0].IsSelected = true;
            list[1].IsSelected = true;
            list[2].IsSelected = true;
            list[3].IsSelected = true;

            // aditional setup...
            list[0]._ObjectType = "Alien-01";
            list[1]._ObjectType = "Moon-01";
            list[2]._ObjectType = "Object-01";
            list[3]._ObjectType = "Planets-01";
            list[0].TargetDrawing = drawing;
            list[1].TargetDrawing = drawing;
            list[2].TargetDrawing = drawing;
            list[3].TargetDrawing = drawing;


            // duplicate everything once
            commandFactory.Create("duplicate", list).Execute();
            Assert.AreEqual(8, drawing.TreeCount);

            // duplicate everything again
            commandFactory.Create("duplicate", list).Execute();
            Assert.AreEqual(12, drawing.TreeCount);

        }

        [TestMethod]
        public void DuplicateCommand_NoSelectedItems()
        {

            Drawing drawing = new Drawing();
            CommandFactory commandFactory = new CommandFactory() { TargetDrawing = drawing };
            drawing.objectFactory = new ObjectFactory() { ReferenceType = typeof(NewCommandTester) };
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Aliens.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Objects.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Moons.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Planets.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Ships.{0}.png");
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Stars.{0}.png");

            // create the 4 objects
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Alien-01", Palette = 0, Location = new Point(10, 10), Size = new Size(80, 80) }, 0));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Moon-01", Palette = 1, Location = new Point(200, 310), Size = new Size(80, 80) }, 1));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Object-01", Palette = 2, Location = new Point(240, 150), Size = new Size(80, 80) }, 2));
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Planets-01", Palette = 3, Location = new Point(350, 300), Size = new Size(80, 80) }, 3));

            // this simulates selecting the 4 items
            List<_Object> list = new List<_Object>();
            list.Add(drawing.FindTreeAtPosition(new Point(10, 10)));
            list.Add(drawing.FindTreeAtPosition(new Point(200, 310)));
            list.Add(drawing.FindTreeAtPosition(new Point(240, 150)));
            list.Add(drawing.FindTreeAtPosition(new Point(350, 300)));

            // duplicate everything once
            commandFactory.Create("duplicate", list).Execute();
            Assert.AreEqual(4, drawing.TreeCount);

        }

        [TestMethod]
        public void DuplicateCommand_undo()
        {

            Drawing drawing = new Drawing();
            CommandFactory commandFactory = new CommandFactory() { TargetDrawing = drawing };
            drawing.objectFactory = new ObjectFactory() { ReferenceType = typeof(NewCommandTester) };
            drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Aliens.{0}.png");

            // create the 1 objects
            drawing.Add(drawing.objectFactory.GetTree(new ObjectExtrinsicState() { TreeType = "Alien-01", Palette = 0, Location = new Point(10, 10), Size = new Size(80, 80) }, 0));
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10, 10)));
            Assert.AreEqual(1, drawing.TreeCount);

            // this simulates selecting the 4 items
            List<_Object> list = new List<_Object>();
            list.Add(drawing.FindTreeAtPosition(new Point(10, 10)));

            // duplication command actually needs the items to be selected
            drawing.FindTreeAtPosition(new Point(10, 10)).IsSelected = true;

            // aditional setup...
            drawing.FindTreeAtPosition(new Point(10, 10))._ObjectType = "Alien-01";
            drawing.FindTreeAtPosition(new Point(10, 10)).TargetDrawing = drawing;

            // duplicate everything once
            Command command = commandFactory.Create("duplicate", list);
            Assert.IsNotNull(command);
            command.Execute();
            Assert.AreEqual(2, drawing.TreeCount);

            // testing undo
            command.undo();
            Assert.IsNotNull(drawing.FindTreeAtPosition(new Point(10, 10)));
            Assert.AreEqual(1, drawing.TreeCount);

        }

    }
}
