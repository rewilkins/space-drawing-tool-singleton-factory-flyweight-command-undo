﻿using System;
using System.IO;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using AppLayer.Command;
using AppLayer.DrawingComponents;
using System.Reflection;
using System.Collections.Generic;
using Microsoft.VisualBasic;

namespace Forests
{
    // NOTE: There some design problems with this class

    public partial class MainForm : Form
    {
        private readonly Drawing _drawing;
        private readonly CommandFactory _commandFactory;

        private string _currentTreeResource;
        private float _currentScale = 1;

        private Point dragStart;
        private Point dragEnd;

        private int numOfPrevObject = 8;

        bool firstCycleRun = false;

        private enum PossibleModes
        {
            None,
            TreeDrawing,
            Selection,
            Move,
            Moving,
            Scale,
            Scaling
        };

        private PossibleModes _mode = PossibleModes.None;

        private Bitmap _imageBuffer;
        private Graphics _imageBufferGraphics;
        private Graphics _panelGraphics;
        private string background;
        private Stream backgroundImageStream;
        private Bitmap backgroundBitmap;

        public MainForm()
        {
            InitializeComponent();
            
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);

            ComboBox_Palette.Items.Add("Aliens");
            ComboBox_Palette.Items.Add("Objects");
            ComboBox_Palette.Items.Add("Moons");
            ComboBox_Palette.Items.Add("Planets");
            ComboBox_Palette.Items.Add("Ships");
            ComboBox_Palette.Items.Add("Stars");

            _drawing = new Drawing();
            _drawing.selectedPalette = 0;

            _commandFactory = new CommandFactory() { TargetDrawing = _drawing };

            _drawing.objectFactory = new ObjectFactory()
            {
                ReferenceType = typeof(Program)
            };
            _drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Aliens.{0}.png");
            _drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Objects.{0}.png");
            _drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Moons.{0}.png");
            _drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Planets.{0}.png");
            _drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Ships.{0}.png");
            _drawing.objectFactory.ResourceNamePattern.Add(@"Forests.Graphics.Stars.{0}.png");

        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            ComputeDrawingPanelSize();
            refreshTimer.Start();
        }

        private void refreshTimer_Tick(object sender, EventArgs e)
        {
            DisplayDrawing();
            toolStripLabel2.Text = CommandProcessor_Singleton.undoStack.Count.ToString();
            toolStripLabel3.Text = CommandProcessor_Singleton.redoStack.Count.ToString();
        }

        private void DisplayDrawing()
        {
            if (backgroundBitmap == null && firstCycleRun == false)
            {
                firstCycleRun = true;
                newButton_Action();
            }

            if (_imageBuffer == null)
            {
                _imageBuffer = new Bitmap(drawingPanel.Width, drawingPanel.Height);
                _imageBufferGraphics = Graphics.FromImage(_imageBuffer);
                _panelGraphics = drawingPanel.CreateGraphics();
            }

            if (backgroundBitmap != null)
                _imageBufferGraphics.DrawImage(backgroundBitmap, new Rectangle(0, 0, drawingPanel.Width, drawingPanel.Height), new Rectangle(0, 0, backgroundBitmap.Width, backgroundBitmap.Height), GraphicsUnit.Pixel);

            if (_drawing.Draw(_imageBufferGraphics))
            {
                _panelGraphics.DrawImageUnscaled(_imageBuffer, 0, 0);
            }

        }

        private void ClearOtherSelectedTools(ToolStripButton ignoreItem)
        {
            foreach (ToolStripItem item in drawingToolStrip.Items)
            {
                ToolStripButton toolButton = item as ToolStripButton;
                if (toolButton != null && item!=ignoreItem && toolButton.Checked )
                    toolButton.Checked = false;
            }
        }

        private void pointerButton_Click(object sender, EventArgs e)
        {
            ToolStripButton button = sender as ToolStripButton;
            pointerButton_Action(button);
        }
        private void pointerButton_Action(ToolStripButton button)
        {
            ClearOtherSelectedTools(button);

            if (button != null && button.Checked)
            {
                _mode = PossibleModes.Selection;
                _currentTreeResource = string.Empty;
            }
            else
            {
                CommandProcessor_Singleton.getInstance().processCommand(_commandFactory.Create("deselect"));
                _mode = PossibleModes.None;
            }
        }

        private void moveButton_Click(object sender, EventArgs e)
        {
            ToolStripButton button = sender as ToolStripButton;
            moveButton_Action(button);
        }
        private void moveButton_Action(ToolStripButton button)
        {
            ClearOtherSelectedTools(button);

            if (button != null && button.Checked)
            {
                _mode = PossibleModes.Move;
                _currentTreeResource = string.Empty;
            }
            else
            {
                CommandProcessor_Singleton.getInstance().processCommand(_commandFactory.Create("deselect"));
                _mode = PossibleModes.None;
            }
        }

        private void scaleButton_Click(object sender, EventArgs e)
        {
            ToolStripButton button = sender as ToolStripButton;
            scaleButton_Action(button);
        }
        private void scaleButton_Action(ToolStripButton button)
        {
            ClearOtherSelectedTools(button);

            if (button != null && button.Checked)
            {
                _mode = PossibleModes.Scale;
                _currentTreeResource = string.Empty;
            }
            else
            {
                CommandProcessor_Singleton.getInstance().processCommand(_commandFactory.Create("deselect"));
                _mode = PossibleModes.None;
            }
        }

        private void treeButton_Click(object sender, EventArgs e)
        {
            ToolStripButton button = sender as ToolStripButton;
            treeButton_Action(button);
        }
        private void treeButton_Action(ToolStripButton button)
        {
            ClearOtherSelectedTools(button);

            if (button != null && button.Checked)
                _currentTreeResource = button.Text;
            else
                _currentTreeResource = string.Empty;

            _mode = (_currentTreeResource != string.Empty) ? PossibleModes.TreeDrawing : PossibleModes.None;
        }

        private void drawingPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (_mode == PossibleModes.Moving)
                if (e.Button == MouseButtons.Left)
                {
                    // add moving effect here
                }
            if (_mode == PossibleModes.Scaling)
                if (e.Button == MouseButtons.Left)
                {
                    // add scaling effect here
                }
        }

        private void drawingPanel_MouseDown(object sender, MouseEventArgs e)
        {
            if (_mode == PossibleModes.Move)
            {
                dragStart = e.Location;
                _mode = PossibleModes.Moving;
            }
            if (_mode == PossibleModes.Scale)
            {
                dragStart = e.Location;
                _mode = PossibleModes.Scaling;
            }
        }

        private void drawingPanel_MouseUp(object sender, MouseEventArgs e)
        {
            if (_mode == PossibleModes.TreeDrawing) //<---
            {
                if (!string.IsNullOrWhiteSpace(_currentTreeResource))
                {
                    CommandProcessor_Singleton.getInstance().processCommand(_commandFactory.Create("add", _currentTreeResource, ComboBox_Palette.SelectedIndex, e.Location, _currentScale));
                }
            }

            else if (_mode == PossibleModes.Selection) //<---
                CommandProcessor_Singleton.getInstance().processCommand(_commandFactory.Create("select", e.Location));

            else if (_mode == PossibleModes.Moving) //<---
            {
                
                dragEnd = e.Location;
                Point locVec = new Point();
                List<_Object> list = _drawing.FindAllSelectedTrees();

                locVec.X = (dragStart.X - dragEnd.X) * -1;
                locVec.Y = (dragStart.Y - dragEnd.Y) * -1;

                CommandProcessor_Singleton.getInstance().processCommand(_commandFactory.Create("move", list, locVec));
                _mode = PossibleModes.Move;
            }
            else if (_mode == PossibleModes.Scaling) //<---
            {
                dragEnd = e.Location;
                List<_Object> list = _drawing.FindAllSelectedTrees();
                CommandProcessor_Singleton.getInstance().processCommand(_commandFactory.Create("scale", list, dragStart, dragEnd));
                _mode = PossibleModes.Scale;
            }
        }

        private void scale_Leave(object sender, EventArgs e)
        {
            _currentScale = ConvertToFloat(scale.Text, 0.01F, 99.0F, 1);
            scale.Text = _currentScale.ToString(CultureInfo.InvariantCulture);
        }

        private float ConvertToFloat(string text, float min, float max, float defaultValue)
        {
            float result = defaultValue;
            if (!string.IsNullOrWhiteSpace(text))
            {
                if (!float.TryParse(text, out result))
                    result = defaultValue;
                else
                    result = Math.Max(min, Math.Min(max, result));
            }
            return result;
        }

        private void scale_TextChanged(object sender, EventArgs e)
        {
            _currentScale = ConvertToFloat(scale.Text, 0.01F, 99.0F, 1);
        }

        private void openButton_Click(object sender, EventArgs e)
        {
            openButton_Action();
        }
        

        private void openButton_Action()
        {
            string fileName = Interaction.InputBox("Enter the filename", "Load Drawing", "", drawingPanel.Location.X, drawingPanel.Location.Y);
            if (!string.IsNullOrWhiteSpace(fileName))
            {
                if (!fileName.EndsWith(".json"))
                    fileName += ".json";

                CommandProcessor_Singleton.getInstance().processCommand(_commandFactory.Create("load", fileName));
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            saveButton_Action();
        }
        private void saveButton_Action()
        {
            string fileName = Interaction.InputBox("Enter the filename", "Save Drawing", "", drawingPanel.Location.X, drawingPanel.Location.Y);
            if (!string.IsNullOrWhiteSpace(fileName))
            {
                if (!fileName.EndsWith(".json"))
                    fileName += ".json";

                CommandProcessor_Singleton.getInstance().processCommand(_commandFactory.Create("save", fileName));
            }
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            ComputeDrawingPanelSize();
        }

        private void ComputeDrawingPanelSize()
        {
            int width = Width - drawingToolStrip.Width;
            int height = Height - fileToolStrip.Height;

            drawingPanel.Size = new Size(width, height);
            drawingPanel.Location = new Point(drawingToolStrip.Width, fileToolStrip.Height);

            _imageBuffer = null;
            if (_drawing != null)
                _drawing.IsDirty = true;
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            deleteButton_Action();
        }
        private void deleteButton_Action()
        {
            CommandProcessor_Singleton.getInstance().processCommand(_commandFactory.Create("Remove"));
        }

        private void duplicateButton_Click(object sender, EventArgs e)
        {
            duplicateButton_Action();
        }
        private void duplicateButton_Action()
        {
            List<_Object> list = _drawing.FindAllSelectedTrees();
            CommandProcessor_Singleton.getInstance().processCommand(_commandFactory.Create("duplicate", list));
        }

        private void ComboBox_Palette_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ActiveControl = drawingPanel;  // this is to prevent hotkeys from typing into the combobox text field

            _drawing.selectedPalette = ComboBox_Palette.SelectedIndex;

            int num = drawingToolStrip.Items.Count - 1;
            int mod = numOfPrevObject - 1;

            for (int i = num; i > numOfPrevObject; i--)
            {
                drawingToolStrip.Items.RemoveAt(i);
            }
            
            if(ComboBox_Palette.SelectedItem.ToString() == "Aliens")
            {
                num = 5;
                for (int i = numOfPrevObject; i < numOfPrevObject + num; i++)
                {
                    ToolStripButton button = new ToolStripButton();

                    button.Text = "Alien-0" + (i - mod);
                    button.DisplayStyle = ToolStripItemDisplayStyle.Image;
                    button.Size = new Size(61, 61);
                    button.AutoSize = false;
                    button.CheckOnClick = true;
                    button.Image = Image.FromFile(@"..\..\Graphics\Aliens\Alien-0" + (i - mod) + ".png");
                    button.ImageTransparentColor = Color.Magenta;
                    button.Click += new EventHandler(treeButton_Click);

                    drawingToolStrip.Items.Add(button);
                }
            }
            if (ComboBox_Palette.SelectedItem.ToString() == "Moons")
            {
                num = 6;
                for (int i = numOfPrevObject; i < numOfPrevObject + num; i++)
                {
                    ToolStripButton button = new ToolStripButton();

                    button.Text = "Moon-0" + (i - mod);
                    button.DisplayStyle = ToolStripItemDisplayStyle.Image;
                    button.Size = new Size(61, 61);
                    button.AutoSize = false;
                    button.CheckOnClick = true;
                    button.Image = Image.FromFile(@"..\..\Graphics\Moons\Moon-0" + (i - mod) + ".png");
                    button.ImageTransparentColor = Color.Magenta;
                    button.Click += new EventHandler(treeButton_Click);

                    drawingToolStrip.Items.Add(button);
                }
            }
            if (ComboBox_Palette.SelectedItem.ToString() == "Objects")
            {
                num = 5;
                for (int i = numOfPrevObject; i < numOfPrevObject + num; i++)
                {
                    ToolStripButton button = new ToolStripButton();

                    button.Text = "Object-0" + (i - mod);
                    button.DisplayStyle = ToolStripItemDisplayStyle.Image;
                    button.Size = new Size(61, 61);
                    button.AutoSize = false;
                    button.CheckOnClick = true;
                    button.Image = Image.FromFile(@"..\..\Graphics\Objects\Object-0" + (i - mod) + ".png");
                    button.ImageTransparentColor = Color.Magenta;
                    button.Click += new EventHandler(treeButton_Click);

                    drawingToolStrip.Items.Add(button);
                }
            }
            if (ComboBox_Palette.SelectedItem.ToString() == "Planets")
            {
                num = 6;
                for (int i = numOfPrevObject; i < numOfPrevObject + num; i++)
                {
                    ToolStripButton button = new ToolStripButton();

                    button.Text = "Planet-0" + (i - mod);
                    button.DisplayStyle = ToolStripItemDisplayStyle.Image;
                    button.Size = new Size(61, 61);
                    button.AutoSize = false;
                    button.CheckOnClick = true;
                    button.Image = Image.FromFile(@"..\..\Graphics\Planets\Planet-0" + (i - mod) + ".png");
                    button.ImageTransparentColor = Color.Magenta;
                    button.Click += new EventHandler(treeButton_Click);

                    drawingToolStrip.Items.Add(button);
                }
            }
            if (ComboBox_Palette.SelectedItem.ToString() == "Ships")
            {
                num = 6;
                for (int i = numOfPrevObject; i < numOfPrevObject + num; i++)
                {
                    ToolStripButton button = new ToolStripButton();

                    button.Text = "Ship-0" + (i - mod);
                    button.DisplayStyle = ToolStripItemDisplayStyle.Image;
                    button.Size = new Size(61, 61);
                    button.AutoSize = false;
                    button.CheckOnClick = true;
                    button.Image = Image.FromFile(@"..\..\Graphics\Ships\Ship-0" + (i - mod) + ".png");
                    button.ImageTransparentColor = Color.Magenta;
                    button.Click += new EventHandler(treeButton_Click);

                    drawingToolStrip.Items.Add(button);
                }
            }
            if (ComboBox_Palette.SelectedItem.ToString() == "Stars")
            {
                num = 4;
                for (int i = numOfPrevObject; i < numOfPrevObject + num; i++)
                {
                    ToolStripButton button = new ToolStripButton();

                    button.Text = "Star-0" + (i - mod);
                    button.DisplayStyle = ToolStripItemDisplayStyle.Image;
                    button.Size = new Size(61, 61);
                    button.AutoSize = false;
                    button.CheckOnClick = true;
                    button.Image = Image.FromFile(@"..\..\Graphics\Stars\Star-0" + (i - mod) + ".png");
                    button.ImageTransparentColor = Color.Magenta;
                    button.Click += new EventHandler(treeButton_Click);

                    drawingToolStrip.Items.Add(button);
                }
            }
            
        }

        private void newButton_Click(object sender, EventArgs e)
        {
            newButton_Action();
        }
        private void newButton_Action()
        {
            NewDrawing form = new NewDrawing();
            var result = form.ShowDialog();
            if (result == DialogResult.OK)
            {
                CommandProcessor_Singleton.getInstance().processCommand(_commandFactory.Create("new"));

                background = @"Forests.Graphics._Backgrounds." + form.background + ".png";

                if (string.IsNullOrWhiteSpace(background)) return;
                var assembly = Assembly.GetAssembly(typeof(Program));
                if (assembly == null) return;
                backgroundImageStream = assembly.GetManifestResourceStream(background);
                if (backgroundImageStream != null)
                {
                    backgroundBitmap = new Bitmap(backgroundImageStream);
                }

            }
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.Control && e.KeyCode == Keys.N)
            {
                newButton_Action();
            }

            if (e.Control && e.KeyCode == Keys.P)
            {
                pointerButton.Checked = !pointerButton.Checked;
                pointerButton_Action(pointerButton);
                
            }

            if (e.Control && e.KeyCode == Keys.M)
            {
                moveButton.Checked = !moveButton.Checked;
                moveButton_Action(moveButton);
            }

            if (e.Control && e.KeyCode == Keys.T)
            {
                scaleButton.Checked = !scaleButton.Checked;
                scaleButton_Action(scaleButton);
            }

            if (e.KeyCode == Keys.D1)
            {
                int mod = 1;
                if (drawingToolStrip.Items.Count > numOfPrevObject + mod)
                {
                    ((ToolStripButton)drawingToolStrip.Items[numOfPrevObject + mod]).Checked = !((ToolStripButton)drawingToolStrip.Items[numOfPrevObject + mod]).Checked;
                    treeButton_Action((ToolStripButton)drawingToolStrip.Items[numOfPrevObject + mod]);
                }
            }

            if (e.KeyCode == Keys.D2)
            {
                int mod = 2;
                if (drawingToolStrip.Items.Count > numOfPrevObject + mod)
                {
                    ((ToolStripButton)drawingToolStrip.Items[numOfPrevObject + mod]).Checked = !((ToolStripButton)drawingToolStrip.Items[numOfPrevObject + mod]).Checked;
                    treeButton_Action((ToolStripButton)drawingToolStrip.Items[numOfPrevObject + mod]);
                }
            }

            if (e.KeyCode == Keys.D3)
            {
                int mod = 3;
                if (drawingToolStrip.Items.Count > numOfPrevObject + mod)
                {
                    ((ToolStripButton)drawingToolStrip.Items[numOfPrevObject + mod]).Checked = !((ToolStripButton)drawingToolStrip.Items[numOfPrevObject + mod]).Checked;
                    treeButton_Action((ToolStripButton)drawingToolStrip.Items[numOfPrevObject + mod]);
                }
            }

            if (e.KeyCode == Keys.D4)
            {
                int mod = 4;
                if (drawingToolStrip.Items.Count > numOfPrevObject + mod)
                {
                    ((ToolStripButton)drawingToolStrip.Items[numOfPrevObject + mod]).Checked = !((ToolStripButton)drawingToolStrip.Items[numOfPrevObject + mod]).Checked;
                    treeButton_Action((ToolStripButton)drawingToolStrip.Items[numOfPrevObject + mod]);
                }
            }

            if (e.KeyCode == Keys.D5)
            {
                int mod = 5;
                if (drawingToolStrip.Items.Count > numOfPrevObject + mod)
                {
                    ((ToolStripButton)drawingToolStrip.Items[numOfPrevObject + mod]).Checked = !((ToolStripButton)drawingToolStrip.Items[numOfPrevObject + mod]).Checked;
                    treeButton_Action((ToolStripButton)drawingToolStrip.Items[numOfPrevObject + mod]);
                }
            }

            if (e.KeyCode == Keys.D6)
            {
                int mod = 6;
                if (drawingToolStrip.Items.Count > numOfPrevObject + mod)
                {
                    ((ToolStripButton)drawingToolStrip.Items[numOfPrevObject + mod]).Checked = !((ToolStripButton)drawingToolStrip.Items[numOfPrevObject + mod]).Checked;
                    treeButton_Action((ToolStripButton)drawingToolStrip.Items[numOfPrevObject + mod]);
                }
            }

            if (e.KeyCode == Keys.D7)
            {
                int mod = 7;
                if (drawingToolStrip.Items.Count > numOfPrevObject + mod)
                {
                    ((ToolStripButton)drawingToolStrip.Items[numOfPrevObject + mod]).Checked = !((ToolStripButton)drawingToolStrip.Items[numOfPrevObject + mod]).Checked;
                    treeButton_Action((ToolStripButton)drawingToolStrip.Items[numOfPrevObject + mod]);
                }
            }

            if (e.KeyCode == Keys.F1)
            {
                ComboBox_Palette.SelectedIndex = 0;
            }

            if (e.KeyCode == Keys.F2)
            {
                ComboBox_Palette.SelectedIndex = 1;
            }

            if (e.KeyCode == Keys.F3)
            {
                ComboBox_Palette.SelectedIndex = 2;
            }

            if (e.KeyCode == Keys.F4)
            {
                ComboBox_Palette.SelectedIndex = 3;
            }

            if (e.KeyCode == Keys.F5)
            {
                ComboBox_Palette.SelectedIndex = 4;
            }

            if (e.KeyCode == Keys.F6)
            {
                ComboBox_Palette.SelectedIndex = 5;
            }

            if (e.Control && e.KeyCode == Keys.O)
            {
                openButton_Action();
            }

            if (e.Control && e.KeyCode == Keys.S)
            {
                saveButton_Action();
            }

            if (e.Control && e.KeyCode == Keys.Z)
            {
                undoButton_Action();
            }

            if (e.Control && e.KeyCode == Keys.Y)
            {
                redoButton_Action();
            }

            if(e.Control && e.KeyCode == Keys.D)
            {
                duplicateButton_Action();
            }

            if (e.KeyCode == Keys.Delete)
            {
                deleteButton_Action();
            }

        }

        private void undoButton_Click(object sender, EventArgs e)
        {
            undoButton_Action();
        }
        private void undoButton_Action()
        {
            CommandProcessor_Singleton.getInstance().undoCommand();
        }

        private void redoButton_Click(object sender, EventArgs e)
        {
            redoButton_Action();
        }
        private void redoButton_Action()
        {
            CommandProcessor_Singleton.getInstance().redoCommand();
        }
    }
}
