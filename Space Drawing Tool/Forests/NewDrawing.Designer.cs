﻿namespace Forests
{
    partial class NewDrawing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Background_01 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.Background_02 = new System.Windows.Forms.Panel();
            this.Background_04 = new System.Windows.Forms.Panel();
            this.Background_03 = new System.Windows.Forms.Panel();
            this.Background_06 = new System.Windows.Forms.Panel();
            this.Background_05 = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // Background_01
            // 
            this.Background_01.BackgroundImage = global::Forests.Properties.Resources.Background_01;
            this.Background_01.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Background_01.Location = new System.Drawing.Point(15, 45);
            this.Background_01.Name = "Background_01";
            this.Background_01.Size = new System.Drawing.Size(236, 153);
            this.Background_01.TabIndex = 0;
            this.Background_01.Click += new System.EventHandler(this.Background_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Select a background:";
            // 
            // Background_02
            // 
            this.Background_02.BackgroundImage = global::Forests.Properties.Resources.Background_02;
            this.Background_02.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Background_02.Location = new System.Drawing.Point(257, 45);
            this.Background_02.Name = "Background_02";
            this.Background_02.Size = new System.Drawing.Size(236, 153);
            this.Background_02.TabIndex = 1;
            this.Background_02.Click += new System.EventHandler(this.Background_Click);
            // 
            // Background_04
            // 
            this.Background_04.BackgroundImage = global::Forests.Properties.Resources.Background_04;
            this.Background_04.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Background_04.Location = new System.Drawing.Point(257, 204);
            this.Background_04.Name = "Background_04";
            this.Background_04.Size = new System.Drawing.Size(236, 153);
            this.Background_04.TabIndex = 3;
            this.Background_04.Click += new System.EventHandler(this.Background_Click);
            // 
            // Background_03
            // 
            this.Background_03.BackgroundImage = global::Forests.Properties.Resources.Background_03;
            this.Background_03.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Background_03.Location = new System.Drawing.Point(15, 204);
            this.Background_03.Name = "Background_03";
            this.Background_03.Size = new System.Drawing.Size(236, 153);
            this.Background_03.TabIndex = 2;
            this.Background_03.Click += new System.EventHandler(this.Background_Click);
            // 
            // Background_06
            // 
            this.Background_06.BackgroundImage = global::Forests.Properties.Resources.Background_06;
            this.Background_06.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Background_06.Location = new System.Drawing.Point(257, 363);
            this.Background_06.Name = "Background_06";
            this.Background_06.Size = new System.Drawing.Size(236, 153);
            this.Background_06.TabIndex = 5;
            this.Background_06.Click += new System.EventHandler(this.Background_Click);
            // 
            // Background_05
            // 
            this.Background_05.BackgroundImage = global::Forests.Properties.Resources.Background_05;
            this.Background_05.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Background_05.Location = new System.Drawing.Point(15, 363);
            this.Background_05.Name = "Background_05";
            this.Background_05.Size = new System.Drawing.Size(236, 153);
            this.Background_05.TabIndex = 4;
            this.Background_05.Click += new System.EventHandler(this.Background_Click);
            // 
            // NewDrawing
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(507, 525);
            this.Controls.Add(this.Background_06);
            this.Controls.Add(this.Background_05);
            this.Controls.Add(this.Background_04);
            this.Controls.Add(this.Background_02);
            this.Controls.Add(this.Background_03);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Background_01);
            this.Name = "NewDrawing";
            this.Text = "NewDrawing";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel Background_01;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel Background_02;
        private System.Windows.Forms.Panel Background_04;
        private System.Windows.Forms.Panel Background_03;
        private System.Windows.Forms.Panel Background_06;
        private System.Windows.Forms.Panel Background_05;
    }
}