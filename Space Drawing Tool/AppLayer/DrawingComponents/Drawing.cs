﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Runtime.Serialization.Json;

namespace AppLayer.DrawingComponents
{
    public class Drawing
    {
        private static readonly DataContractJsonSerializer JsonSerializer = new DataContractJsonSerializer(typeof(List<ObjectExtrinsicState>));

        private readonly List<_Object> _trees = new List<_Object>();

        private readonly object _myLock = new object();

        public ObjectFactory objectFactory { get; set; }

        public _Object SelectedVar { get; set; }
        public bool IsDirty { get; set; }
        public int TreeCount => _trees.Count;

        public int selectedPalette { get; set; }

        public void Clear()
        {
            lock (_myLock)
            {
                _trees.Clear();
                IsDirty = true;
            }
        }

        public void Add(_Object tree)
        {
            if (tree != null)
            {
                lock (_myLock)
                {
                    _trees.Add(tree);
                    IsDirty = true;
                }
            }
        }

        public void RemoveTree(_Object tree)
        {
            if (tree != null)
            {
                lock (_myLock)
                {
                    if (SelectedVar == tree)
                        SelectedVar = null;
                    _trees.Remove(tree);
                    IsDirty = true;
                }
            }
        }

        public _Object FindTreeAtPosition(Point location)
        {
            _Object result;
            lock (_myLock)
            {
                result = _trees.FindLast(t => location.X >= t.Location.X &&
                                              location.X < t.Location.X + t.Size.Width &&
                                              location.Y >= t.Location.Y &&
                                              location.Y < t.Location.Y + t.Size.Height);
            }
            return result;
        }

        public List<_Object> FindAllSelectedTrees()
        {
            List<_Object> result = new List<_Object>();
            foreach (var item in _trees)
            {
                if( item.IsSelected == true )
                    result.Add(item);
            }
            return result;
        }

        public void DeselectAll()
        {
            lock (_myLock)
            {
                foreach (var t in _trees)
                    t.IsSelected = false;
                IsDirty = true;
            }    
        }

        public void DeleteAllSelected()
        {
            lock (_myLock)
            {
                _trees.RemoveAll(t => t.IsSelected);
                IsDirty = true;
            }
        }

        public void DeleteAtLocation(Point location)
        {
            lock (_myLock)
            {
                _trees.RemoveAll(t => t.Location == location);
                IsDirty = true;
            }
        }

        public bool Draw(Graphics graphics)
        {
            bool didARedraw = false;
            lock (_myLock)
            {
                if (IsDirty)
                {
                    //graphics.Clear(Color.Transparent);
                    foreach (var t in _trees)
                        t.Draw(graphics);
                    IsDirty = false;
                    didARedraw = true;
                }
            }
            return didARedraw;
        }

        public void LoadFromStream(Stream stream)
        {
            if (stream == null) return;

            lock (_myLock)
            {
                _trees.Clear();

                var extrinsicStates = JsonSerializer.ReadObject(stream) as List<ObjectExtrinsicState>;

                foreach (ObjectExtrinsicState extrinsicState in extrinsicStates)
                {
                    _Object tree = objectFactory.GetTree(extrinsicState, extrinsicState.Palette);
                    _trees.Add(tree);
                }
                
                IsDirty = true;
            }
        }

        public void SaveToStream(Stream stream)
        {
            
            List<ObjectExtrinsicState> extrinsicStates = new List<ObjectExtrinsicState>();
            lock (_myLock)
            {
                foreach (ObjectWithAllState tree in _trees)
                {
                    if (tree != null)
                        extrinsicStates.Add(tree.ExtrinsicStatic);
                }
            }
            JsonSerializer.WriteObject(stream, extrinsicStates);
            
        }

        public List<_Object> getItemList()
        {
            return _trees;
        }

    }
}
