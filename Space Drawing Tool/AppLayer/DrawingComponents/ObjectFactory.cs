﻿using System;
using System.Collections.Generic;

namespace AppLayer.DrawingComponents
{
    public class ObjectFactory
    {
        public List<string> ResourceNamePattern = new List<string>();
        public Type ReferenceType { get; set; }

        private readonly Dictionary<string, ObjectWithIntrinsicState> _sharedTrees = new Dictionary<string, ObjectWithIntrinsicState>();

        public ObjectWithAllState GetTree(ObjectExtrinsicState extrinsicState, int palette)
        {
            string resourceName = string.Format(ResourceNamePattern[palette], extrinsicState.TreeType);

            ObjectWithIntrinsicState treeWithIntrinsicState;
            if (_sharedTrees.ContainsKey(extrinsicState.TreeType))
                treeWithIntrinsicState = _sharedTrees[extrinsicState.TreeType];
            else
            {
                treeWithIntrinsicState = new ObjectWithIntrinsicState();
                treeWithIntrinsicState.LoadFromResource(resourceName, ReferenceType);
                _sharedTrees.Add(extrinsicState.TreeType, treeWithIntrinsicState);
            }

            return new ObjectWithAllState(treeWithIntrinsicState, extrinsicState);
        }
    }
}
