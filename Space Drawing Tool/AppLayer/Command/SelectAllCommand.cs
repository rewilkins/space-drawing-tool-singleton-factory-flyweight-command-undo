﻿using System.Drawing;
using AppLayer.DrawingComponents;
using System;

namespace AppLayer.Command
{

    //
    //  THIS IS NOT IMPLEMENTED YET
    //

    public class SelectAllCommand : Command
    {
        private readonly Point _location;
         
        internal SelectAllCommand(params object[] commandParameters)
        {
            // store all the items that were already selected
            if (commandParameters.Length>0)
                _location = (Point) commandParameters[0];
        }

        public override void Execute()
        {
            var tree = TargetDrawing?.FindTreeAtPosition(_location);
            if (tree != null)
            {
                tree.IsSelected = !tree.IsSelected;
                TargetDrawing.IsDirty = true;
            }
        }

        public override void undo()
        {
            // not implemented yet
        }
    }
}
