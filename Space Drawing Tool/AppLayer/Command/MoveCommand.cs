﻿using System.Drawing;
using AppLayer.DrawingComponents;
using System.Collections.Generic;
using System;

namespace AppLayer.Command
{
    public class MoveCommand : Command
    {
        private List<_Object> items = new List<_Object>();
        private List<Point> _startLocation = new List<Point>();
        private List<Point> _endLocation = new List<Point>();
        private Point vec;

        internal MoveCommand(params object[] commandParameters)
        {
            if (commandParameters.Length > 0)
            {
                items = (List<_Object>)commandParameters[0];
                foreach (_Object item in items)
                {
                    _startLocation.Add(item.Location);
                }
            }
            if (commandParameters.Length > 1)
            {
                vec = (Point)commandParameters[1];
                for (int i = 0; i < items.Count; i++)
                {
                    _endLocation.Add(new Point(_startLocation[i].X + vec.X, _startLocation[i].Y + vec.Y));
                }
                
            }
        }

        public override void Execute()
        {
            for (int i = 0; i < items.Count; i++)
            {
                TargetDrawing.FindTreeAtPosition(_startLocation[i]).Location = _endLocation[i];
            }
            TargetDrawing.IsDirty = true;

        }

        public override void undo()
        {
            for (int i = 0; i < items.Count; i++)
            {
                TargetDrawing.FindTreeAtPosition(_endLocation[i]).Location = _startLocation[i];
            }
            TargetDrawing.IsDirty = true;
        }
    }
}
