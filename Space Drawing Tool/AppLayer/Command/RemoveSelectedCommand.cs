﻿using AppLayer.DrawingComponents;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace AppLayer.Command
{
    public class RemoveSelectedCommand : Command
    {
        List<string> removedItemsResource = new List<string>();
        List<Point> removedItemsLocations = new List<Point>();
        List<Size> removedItemsSize = new List<Size>();
        List<int> removedItemPalette = new List<int>();
        List<string> removedItemType = new List<string>();

        internal RemoveSelectedCommand() { }

        public override void Execute()
        {
            List<_Object> selectedList = TargetDrawing?.FindAllSelectedTrees();

            foreach(_Object item in selectedList)
            {
                removedItemsResource.Add(item._ObjectType);
                removedItemsLocations.Add(item.Location);
                removedItemsSize.Add(item.Size);
                removedItemPalette.Add(item._Palette);
                removedItemType.Add(item._ObjectType);
            }

            TargetDrawing?.DeleteAllSelected();
        }

        public override void undo()
        {
            int total = removedItemsResource.Count;
            for (int i = total - 1; i >= 0; i--)
            {
                Point newLoc = new Point(removedItemsLocations[i].X + removedItemsSize[i].Width / 2, removedItemsLocations[i].Y + removedItemsSize[i].Height / 2);
                Command command = new AddCommand(removedItemsResource[i], removedItemPalette[i], newLoc, removedItemsSize[i]);
                command.TargetDrawing = TargetDrawing;
                command.Execute();
                TargetDrawing.FindTreeAtPosition(newLoc).IsSelected = true;

                removedItemsResource.RemoveAt(i);
                removedItemsLocations.RemoveAt(i);
                removedItemsSize.RemoveAt(i);
                removedItemPalette.RemoveAt(i);
                removedItemType.RemoveAt(i);
            }
            TargetDrawing.IsDirty = true;
        }
    }
}
