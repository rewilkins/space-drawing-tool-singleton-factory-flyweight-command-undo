﻿using System;
using AppLayer.DrawingComponents;
using System.Collections.Generic;

namespace AppLayer.Command
{
    public class DeselectAllCommand : Command
    {
        List<_Object> selectedList = new List<_Object>();

        internal DeselectAllCommand() { }

        public override void Execute()
        {
            selectedList = TargetDrawing?.FindAllSelectedTrees();
            TargetDrawing?.DeselectAll();
        }

        public override void undo()
        {
            foreach (_Object item in selectedList)
            {
                item.IsSelected = true;
            }
        }
    }
}
