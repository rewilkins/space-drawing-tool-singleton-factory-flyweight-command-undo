﻿using Amazon.S3;
using Amazon.S3.Model;
using System;
using System.IO;

namespace AppLayer.Command
{
    public class LoadCommand : Command
    {
        private readonly string _filename;

        internal LoadCommand() { }
        internal LoadCommand(params object[] commandParameters)
        {
            if (commandParameters.Length > 0)
                _filename = commandParameters[0] as string;
        }

        public override void Execute()
        {
            //StreamReader reader = new StreamReader(_filename);
            //TargetDrawing?.LoadFromStream(reader.BaseStream);
            //reader.Close();

            if (string.IsNullOrWhiteSpace(_filename) || TargetDrawing == null) return;

            try
            {
                AmazonS3Client client = new AmazonS3Client();
                GetObjectRequest request = new GetObjectRequest()
                {
                    BucketName = "rob-wilkins-cs5600-hw3",
                    Key = "HW3/" + _filename
                };

                using (GetObjectResponse response = client.GetObject(request))
                {
                    TargetDrawing?.LoadFromStream(response.ResponseStream);
                }
            }

            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    Console.WriteLine("Please check the provided AWS Credentials.");
                    Console.WriteLine("If you haven't signed up for Amazon S3, please visit http://aws.amazon.com/s3");
                }
                else
                {
                    Console.WriteLine("An error occurred with the message '{0}' when reading an object", amazonS3Exception.Message);
                }
            }

        }

        public override void undo()
        {
            throw new NotImplementedException();
        }
    }
}
