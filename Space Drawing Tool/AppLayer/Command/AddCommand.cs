﻿using System;
using System.Drawing;
using AppLayer.DrawingComponents;

namespace AppLayer.Command
{
    public class AddCommand : Command
    {
        private int NormalWidth = 80;
        private int NormalHeight = 80;

        Point treeLocation = new Point();

        private readonly string _treeType;
        private readonly int targetPalette;
        private Point _location;
        private readonly float _scale;
        internal AddCommand() { }

        /// <summary>
        /// Constructor
        /// 
        /// </summary>
        /// <param name="commandParameters">An array of parameters, where
        ///     [1]: string     tree type -- a fully qualified resource name
        ///     [2]: Point      center location for the tree, defaut = top left corner
        ///     [3]: float      scale factor</param>
        internal AddCommand(params object[] commandParameters)
        {
            if (commandParameters.Length > 0)
            {
                _treeType = commandParameters[0] as string;
            }

            if (commandParameters.Length > 1)
            {
                targetPalette = (int)commandParameters[1];
            }

            if (commandParameters.Length > 2)
                _location = (Point) commandParameters[2];
            else
                _location = new Point(0, 0);

            if (commandParameters.Length > 3 && commandParameters[3] is float)
                _scale = (float) commandParameters[3];
            else
                _scale = 1.0F;

            if (commandParameters.Length > 3 && commandParameters[3] is Size)
            {
                NormalWidth = ((Size)commandParameters[3]).Width;
                NormalHeight = ((Size)commandParameters[3]).Height;
            }
            else
            {
                NormalWidth = 80;
                NormalHeight = 80;
            }
        }

        public override void Execute()
        {
            if (string.IsNullOrWhiteSpace(_treeType) || TargetDrawing==null) return;

            Size treeSize = new Size()
            {
                Width = Convert.ToInt16(Math.Round(NormalWidth * _scale, 0)),
                Height = Convert.ToInt16(Math.Round(NormalHeight * _scale, 0))
            };
            treeLocation = new Point(_location.X - treeSize.Width / 2, _location.Y - treeSize.Height / 2);


            ObjectExtrinsicState extrinsicState = new ObjectExtrinsicState()
            {
                TreeType = _treeType,
                Palette = targetPalette,
                Location = treeLocation,
                Size = treeSize
            };

            _Object tree = TargetDrawing.objectFactory.GetTree(extrinsicState, targetPalette);
            tree._ObjectType = _treeType;
            tree._Palette = targetPalette;
            TargetDrawing.Add(tree);
            
        }

        public override void undo()
        {
            TargetDrawing?.DeleteAtLocation(treeLocation);
        }
    }
}
