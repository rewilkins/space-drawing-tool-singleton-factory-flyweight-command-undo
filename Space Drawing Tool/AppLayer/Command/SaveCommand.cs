﻿using System.IO;
using System.Runtime.Serialization.Json;
using AppLayer.DrawingComponents;
using System;
using Amazon.S3;
using System.Collections.Generic;
using Amazon.S3.Model;
using System.Text;

namespace AppLayer.Command
{
    public class SaveCommand : Command
    {
        private readonly string _filename;
        internal SaveCommand(params object[] commandParameters)
        {
            if (commandParameters.Length > 0)
                _filename = commandParameters[0] as string;
        }

        public override void Execute()
        {
            //StreamWriter writer = new StreamWriter(_filename);
            //TargetDrawing?.SaveToStream(writer.BaseStream);
            //writer.Close();

            if (string.IsNullOrWhiteSpace(_filename) || TargetDrawing == null) return;

            try
            {
                AmazonS3Client client = new AmazonS3Client();
                MemoryStream stream = new MemoryStream();

                TargetDrawing?.SaveToStream(stream);
                string content = Encoding.ASCII.GetString(stream.ToArray());

                PutObjectRequest request = new PutObjectRequest()
                {
                    ContentBody = content,
                    BucketName = "rob-wilkins-cs5600-hw3",
                    Key = "HW3/" + _filename
                };

                PutObjectResponse rewponse = client.PutObject(request);

            }

            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    Console.WriteLine("Please check the provided AWS Credentials.");
                    Console.WriteLine("If you haven't signed up for Amazon S3, please visit http://aws.amazon.com/s3");
                }
                else
                {
                    Console.WriteLine("An error occurred with the message '{0}' when writing an object", amazonS3Exception.Message);
                }
            }


        }

        public override void undo()
        {
            throw new NotImplementedException();
        }
    }
}
