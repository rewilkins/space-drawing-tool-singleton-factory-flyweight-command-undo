﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppLayer.Command
{
    public class CommandProcessor_Singleton
    {
        private volatile static CommandProcessor_Singleton uniqueInstance;
        private readonly static object _myLock = new object();

        public static Stack<Command> undoStack = new Stack<Command>();
        public static Stack<Command> redoStack = new Stack<Command>();

        private int commandCount = 0;

        private CommandProcessor_Singleton() { }

        public static CommandProcessor_Singleton getInstance()
        {
            if (uniqueInstance == null)
            {
                lock (_myLock)
                {
                    if (uniqueInstance == null)
                    {
                        uniqueInstance = new CommandProcessor_Singleton();
                    }
                }
            }
            return uniqueInstance; 
        }

        public void processCommand(Command command)
        {
            if (command.commandType != "SAVE")
            {
                if (command.commandType != "LOAD")
                {
                    if (command.commandType == "NEW")
                    {
                        undoStack.Clear();
                        redoStack.Clear();
                    }
                    else
                    {
                        undoStack.Push(command);
                        if (undoStack.Count + redoStack.Count != commandCount) redoStack.Clear();
                    }
                    command.Execute();
                }
            }
            if (command.commandType == "SAVE" || command.commandType == "LOAD")
            {
                command.Execute();
            }
        }

        public void undoCommand()
        {
            if (undoStack.Count > 0)
            {
                Command command = undoStack.Pop();
                redoStack.Push(command);
                command.undo();
            }
            commandCount = undoStack.Count + redoStack.Count;
        }

        public void redoCommand()
        {
            if (redoStack.Count > 0)
            {
                Command command = redoStack.Pop();
                undoStack.Push(command);
                command.Execute();
            }
        }
    }
}
