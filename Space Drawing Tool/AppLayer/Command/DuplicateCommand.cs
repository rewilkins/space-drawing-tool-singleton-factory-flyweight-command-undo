﻿using System;
using System.Drawing;
using AppLayer.DrawingComponents;
using System.Collections.Generic;

namespace AppLayer.Command
{
    public class DuplicateCommand : Command
    {
        private List<_Object> duppedFrom = new List<_Object>();
        private List<_Object> duppedList = new List<_Object>();

        private const int NormalWidth = 80;
        private const int NormalHeight = 80;

        private string _treeType;
        internal DuplicateCommand() { }

        /// <summary>
        /// Constructor
        /// 
        /// </summary>
        /// <param name="commandParameters">An array of parameters, where
        ///     [1]: string     tree type -- a fully qualified resource name
        ///     [2]: Point      center location for the tree, defaut = top left corner
        ///     [3]: float      scale factor</param>
        internal DuplicateCommand(params object[] commandParameters)
        {
            if (commandParameters.Length > 0)
            {
                // this was moved down into the execute statment so patch an error with undo/redo
                //duppedFrom = (List<_Object>)commandParameters[0];
            }
        }

        public override void Execute()
        {

            duppedFrom = TargetDrawing.FindAllSelectedTrees();
            int total = duppedFrom.Count;
            for (int i = 0; i < total; i++)
            {

                _treeType = duppedFrom[i]._ObjectType;
                if (string.IsNullOrWhiteSpace(_treeType) || TargetDrawing == null) return;

                duppedFrom[i].IsSelected = false;

                Size treeSize = duppedFrom[i].Size;
                Point treeLocation = new Point(duppedFrom[i].Location.X + duppedFrom[i].Size.Width - 25, duppedFrom[i].Location.Y + duppedFrom[i].Size.Height - 25);

                ObjectExtrinsicState extrinsicState = new ObjectExtrinsicState()
                {
                    TreeType = _treeType,
                    Palette = duppedFrom[i]._Palette,
                    Location = treeLocation,
                    Size = treeSize
                };


                _Object tree = TargetDrawing.objectFactory.GetTree(extrinsicState, duppedFrom[i]._Palette);
                tree._ObjectType = duppedFrom[i]._ObjectType;
                tree._Palette = duppedFrom[i]._Palette;
                tree.IsSelected = true;
                duppedList.Add(tree);
                TargetDrawing.Add(tree);
                
            }

            TargetDrawing.IsDirty = true;
        }

        public override void undo()
        {
            int total = duppedList.Count;
            for (int i = total - 1; i >= 0; i--)
            {
                TargetDrawing?.DeleteAtLocation(duppedList[i].Location);
                TargetDrawing.FindTreeAtPosition(duppedFrom[i].Location).IsSelected = true;
                duppedList.RemoveAt(i);
            }
            TargetDrawing.IsDirty = true;
        }
    }
}
