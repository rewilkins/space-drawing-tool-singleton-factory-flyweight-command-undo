﻿using System.Drawing;
using AppLayer.DrawingComponents;
using System.Collections.Generic;
using System;

namespace AppLayer.Command
{
    public class ScaleCommand : Command
    {
        private List<_Object> items = new List<_Object>();
        private List<Point> _startLocation = new List<Point>();
        private List<Size> _startSize = new List<Size>();
        private List<float> _scaleX = new List<float>();
        private List<float> _scaleY = new List<float>();

        private Point dragStart;
        private Point dragEnd;
        private float _tempScaleX;
        private float _tempScaleY;

        internal ScaleCommand(params object[] commandParameters)
        {

            if (commandParameters.Length > 1)
                dragStart = (Point)commandParameters[1];

            if (commandParameters.Length > 2)
                dragEnd = (Point)commandParameters[2];

            if (commandParameters.Length > 0)
            {
                items = (List<_Object>)commandParameters[0];
                foreach (_Object item in items)
                { 

                    _startLocation.Add(item.Location);
                    _startSize.Add(item.Size);

                    _tempScaleX = 1;
                    _tempScaleY = 1;

                    if (dragEnd.X > dragStart.X)
                    {
                        _tempScaleX = item.Size.Width;
                        _tempScaleX += Math.Abs(((dragStart.X - dragEnd.X) * -1));
                        _tempScaleX /= item.Size.Width;
                    }
                    if (dragEnd.Y > dragStart.Y)
                    {
                        _tempScaleY = item.Size.Height;
                        _tempScaleY += Math.Abs(((dragStart.Y - dragEnd.Y) * -1));
                        _tempScaleY /= item.Size.Height;
                    }

                    if (dragEnd.X < dragStart.X)
                    {
                        _tempScaleX = item.Size.Width;
                        _tempScaleX -= Math.Abs(((dragStart.X - dragEnd.X) * -1));
                        _tempScaleX /= item.Size.Width;
                    }
                    if (dragEnd.Y < dragStart.Y)
                    {
                        _tempScaleY = item.Size.Height;
                        _tempScaleY -= Math.Abs(((dragStart.Y - dragEnd.Y) * -1));
                        _tempScaleY /= item.Size.Height;
                    }

                    _scaleX.Add(_tempScaleX);
                    _scaleY.Add(_tempScaleY);

                }
            }
        }

        public override void Execute()
        {
            for(int i = 0; i < items.Count; i++)
            {
                int tempW = Convert.ToInt16(Math.Round(_startSize[i].Width * _scaleX[i], 0));
                int tempH = Convert.ToInt16(Math.Round(_startSize[i].Height * _scaleY[i], 0));
                if (tempW < 1) tempW = 1;
                if (tempH < 1) tempH = 1;

                Size newSize = new Size() { Width = tempW, Height = tempH };
                TargetDrawing.FindTreeAtPosition(_startLocation[i]).Size = newSize;
            }
            TargetDrawing.IsDirty = true;
        }

        public override void undo()
        {
            for (int i = 0; i < items.Count; i++)
            {
                TargetDrawing.FindTreeAtPosition(_startLocation[i]).Size = _startSize[i];
            }
            TargetDrawing.IsDirty = true;
        }
    }
}
