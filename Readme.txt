Hotkeys:

F1 = Select Palette 1
F2 = Select Palette 2 
F3 = Select Palette 3
F4 = Select Palette 4
F5 = Select Palette 5
F6 = Select Palette 6

1 = Select Item 1 in current palette
2 = Select Item 2 in current palette
3 = Select Item 3 in current palette
4 = Select Item 4 in current palette
5 = Select Item 5 in current palette
6 = Select Item 6 in current palette
ect...

ctrl + "P" = select the selecting tool
ctrl + "M" = select the moving tool
ctrl + "T" = select the scaling tool

ctrl + "N" = create a new drawing
ctrl + "O" = open a existing drawing from the cloud
ctrl + "S" = save the current drawing to the cloud
ctrl + "Z" = invoke the undo
ctrl + "Y" = invoke the redo

ctrl + "D" = duplicate the selected items
Delete = delete the selected items

-----------------------------------------------------------------------
To perform a move command, select the move tool then move the mouse to the drawing panel.
left mouse down records the starting position.
left mouse up records the ending position.
All the selected objects will then be moved exactly the distance and direction the mouse moved.

-----------------------------------------------------------------------
To perform a scale command, select the scale tool then move the mouse to the drawing panel.
left mouse down records the starting position.
left mouse up records the ending position.
All the selected objects will then be scaled appropriately.
moving the mouse down and right will grow the selected objects
moving the mouse up and left will shrink the selected objects